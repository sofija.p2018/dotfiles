My Personal Dotfiles
=====================

# What's in here

There are four parts here

* ~/.config in /config

* ~/ in /userdir

* Window Manager, in their own directories

* Standalone Parts, and Terminal Ricing, in their own directories

-- -- --

# Theme

*	XFCE4

*	Fluxbox

*	Openbox

-- -- --

#  Terminal Ricing Screenshots

* [README Terminal Ricing][readme-terminal]

  ![terminal ricing][terminal-ricing]

-- -- --

#  Standalone Part Screenshots

* [README Standalone][readme-standalone]

  ![Dzen2 Conky][dzen2-conky]

  ![CLI Conky][cli-conky]

-- -- --

#  Window Manager Screenshots

* [README Fluxbox][readme-fluxbox]

* [README Openbox][readme-openbox]

* [README Awesome][readme-awesome]

  ![Awesome Wibox][wibox-all]

* [README XMonad][readme-xmonad]

  ![XMonad DZen2][dzen-all]

* [README i3][readme-i3]

  ![i3-gaps i3status Conky Lua JSON][i3status-all]
  
-- -- --

# ~/.config

## Ricing:

* Midnight Commander

* Powerline

* Rofi

-- -- --

# Userdir

* mpd, ncmpcpp

* yaourtrc

-- -- --

# Changes

2018 June: moved to gitlab

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[readme-terminal]:   https://gitlab.com/epsi-rns/dotfiles/blob/master/terminal/README.md
[readme-standalone]: https://gitlab.com/epsi-rns/dotfiles/blob/master/standalone/README.md

[readme-fluxbox]:    https://gitlab.com/epsi-rns/dotfiles/blob/master/fluxbox/README.md
[readme-openbox]:    https://gitlab.com/epsi-rns/dotfiles/blob/master/openbox/README.md
[readme-awesome]:    https://gitlab.com/epsi-rns/dotfiles/blob/master/awesome/3.5/README.md
[readme-xmonad]:     https://gitlab.com/epsi-rns/dotfiles/blob/master/xmonad/README.md
[readme-i3]:         https://gitlab.com/epsi-rns/dotfiles/blob/master/i3/README.md

[wibox-all]:         https://gitlab.com/epsi-rns/dotfiles/raw/master/awesome/3.5/readme/wibox-all.png
[dzen-all]:          https://gitlab.com/epsi-rns/dotfiles/raw/master/xmonad/readme/dzen-all.png
[i3status-all]:      https://gitlab.com/epsi-rns/dotfiles/raw/master/i3/readme/conky-lua-json-dark-both.png

[cli-conky]:         https://gitlab.com/epsi-rns/dotfiles/raw/master/standalone/readme/cli-conky-lua.png
[dzen2-conky]:       https://gitlab.com/epsi-rns/dotfiles/raw/master/standalone/readme/dzen2-bash-red-deco.png

[terminal-ricing]:      https://gitlab.com/epsi-rns/dotfiles/raw/master/terminal/readme/terminal-ricing.png

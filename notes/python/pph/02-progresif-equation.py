# penghasilan kena pajak
id_pkps = [0, 50, 250, 500, 1000]
id_pkps = [x * 10**6 for x in id_pkps]

# pajak penghasilan = tarif progresif pasal 17
[t1, t2, t3] = [x * 10**6 for x in [50, 250, 500]]
def pph(pkp):
  if pkp <= t1:
    return pkp * 0.05
  elif pkp <= t2:
    return (pkp - t1) * 0.15 + pph(t1)
  elif pkp <= t3:
    return (pkp - t2) * 0.25 + pph(t2)
  else:
    return (pkp - t3) * 0.30 + pph(t3)

for id_pkp in id_pkps:
  id_pph = pph(id_pkp)
  print(f'Rp. {id_pph:18,.2f}')

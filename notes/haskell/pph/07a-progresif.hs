-- pemula haskell
import Data.Foldable

-- penghasilan kena pajak
pkps :: [Int]
pkps = [50*10^6, 250*10^6, 500*10^6, 10^9]

-- pajak penghasilan = tarif progresif pasal 17
pph_recursive :: Double -> Double
pph_recursive pkp_real = pkp_guard pkp_real
  where
    pkp_guard pkp_real
      | pkp_real == 0 = 0
      | otherwise     = (pkp_real - tm) * p + pph_recursive(tm)
    -- [(tingkat, persentase, pengurang)]
    reff    = [(0, 0.05), (50, 0.15), (250, 0.25), (500, 0.30)]
    indices = [ i | (i, (t, p)) <- zip [0..] reff, pkp_real > t * (10^6)]
    (t, p)  = (reff !! last indices)
    tm      = t * (10^6)

main = forM_ pkps (print . round . pph_recursive . fromIntegral)

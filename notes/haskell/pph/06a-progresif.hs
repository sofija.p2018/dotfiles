-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 10^9

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round (fromIntegral pkp * a - b * (10^6))
  where
    range  = [(0, 0), (1, 50), (2, 250), (3, 500)]
    reff   = [(0.05, 0), (0.15, 5), (0.25, 30), (0.30, 55)]
    index  = last [ i | (i, t) <- range, pkp >= (t * 10^6) ]
    (a, b) = reff !! index

main = do
   print $ pph pkp

-- pemula haskell

-- penghasilan kena pajak
pkps :: [Int]
pkps = [50*10^6, 250*10^6, 500*10^6, 10^9]

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round $ maximum $  
  [ fromIntegral pkp * a - b * (10^6)
  | (a, b) <- [(0.05, 0), (0.15, 5), (0.25, 30), (0.30, 55)]
  ]

main = mapM_ (print . pph) pkps

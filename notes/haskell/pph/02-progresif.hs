-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = round(pkpr pkp)
  where
    pkp_d  = fromIntegral(pkp)
    pkpr pkp
      | pkp <=  50000000 = pkp_d * 0.05
      | pkp <= 250000000 = pkp_d * 0.15 -  5000000
      | pkp <= 500000000 = pkp_d * 0.25 - 30000000
      | otherwise        = pkp_d * 0.30 - 55000000

main = print $ pph pkp

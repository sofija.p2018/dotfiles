-- penghasilan kena pajak
pkp :: Double
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Double -> Double
pph pkp
    | pkp <=  50000000 = pkp * 0.05
    | pkp <= 250000000 = pkp * 0.15 -  5000000
    | pkp <= 500000000 = pkp * 0.25 - 30000000
    | otherwise        = pkp * 0.30 - 55000000

main = do
   print $ round $ pph pkp

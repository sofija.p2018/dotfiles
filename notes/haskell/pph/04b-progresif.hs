-- pemula haskell

pkp :: Int
pkp = 1000000000

pph_t = [0, 50, 250, 500]        -- ambang batas (unused)
pph_p = [0.05, 0.15, 0.25, 0.30] -- persentase tarif
pph_d = [0, 5, 30, 55]           -- pengurang, versi singkat

juta = 1000000

pph_t1 :: Int -> Double
pph_t1 pkp = fromIntegral(pkp) * (pph_p !! 0)
          - fromIntegral((pph_d !! 0) * juta)

pph_t2 :: Int -> Double
pph_t2 pkp = fromIntegral(pkp) * (pph_p !! 1) 
           - fromIntegral((pph_d !! 1) * juta)

pph_t3 :: Int -> Double
pph_t3 pkp = fromIntegral(pkp) * (pph_p !! 2) 
           - fromIntegral((pph_d !! 2) * juta)

pph_t4 :: Int -> Double
pph_t4 pkp = fromIntegral(pkp) * (pph_p !! 3) 
           - fromIntegral((pph_d !! 3) * juta)

list_i :: [Int]
list_i = [round(x) | 
                x <- [pph_t1 pkp, pph_t2 pkp, pph_t3 pkp, pph_t4 pkp]]

main = do
   print $ round(pph_t1 pkp)
   print $ round(pph_t2 pkp)
   print $ round(pph_t3 pkp)
   print $ round(pph_t4 pkp)
   putStrLn ""
   print $ maximum $ list_i

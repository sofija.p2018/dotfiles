-- pemula haskell

pkp :: Int
pkp = 1000000000

(t1, t2, t3, t4) = (0, 50, 250, 500)        -- ambang batas (unused)
(p1, p2, p3, p4) = (0.05, 0.15, 0.25, 0.30) -- persentase tarif
(d1, d2, d3, d4) = (0, 5, 30, 55)           -- pengurang, versi singkat
juta = 1000000

pph_t1 :: Int -> Double
pph_t1 pkp = fromIntegral(pkp) * p1 - fromIntegral(d1*juta)

pph_t2 :: Int -> Double
pph_t2 pkp = fromIntegral(pkp) * p2 - fromIntegral(d2*juta)

pph_t3 :: Int -> Double
pph_t3 pkp = fromIntegral(pkp) * p3 - fromIntegral(d3*juta)

pph_t4 :: Int -> Double
pph_t4 pkp = fromIntegral(pkp) * p4 - fromIntegral(d4*juta)

list_pph :: [Int]
list_pph = [ round(pph_t1 pkp)
           , round(pph_t2 pkp)
           , round(pph_t3 pkp)
           , round(pph_t4 pkp)]

list_d :: [Double]
list_d = [pph_t1 pkp, pph_t2 pkp, pph_t3 pkp, pph_t4 pkp]

round_list :: [Double] -> [Int]
round_list d = [round(x) | x <- d]

main = do
   print $ round(pph_t1 pkp)
   print $ round(pph_t2 pkp)
   print $ round(pph_t3 pkp)
   print $ round(pph_t4 pkp)
   putStrLn ""
   print $ maximum $ round_list list_d

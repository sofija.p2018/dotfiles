-- pemula haskell

-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

-- [(persentase, pengurang)]
skemaTarif :: [(Double, Int)]
skemaTarif =
    [(0.05,  0)
    ,(0.15,  5)
    ,(0.25, 30)
    ,(0.30, 55)
    ]

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp = maximum $ [round(x) | x <- pph_list]
    where
      -- Typecast, misalnya ke Double
      pkp_d  = fromIntegral(pkp)
      -- untuk menghindari error menulis digit
      juta   = 1000000
      -- pengurang, versi dalam juta rupiah
      dj x = fromIntegral(x * juta)
      -- daftar tingkat, index dari nol
      tingkat = [0, 1, 2, 3]
      -- perhitungan utama, semua tingkat dihitung
      pph_list = [pkp_d * fst(skemaTarif !! x) - dj(snd(skemaTarif !! x)) | x <- tingkat]

main = do
   print $ pph pkp

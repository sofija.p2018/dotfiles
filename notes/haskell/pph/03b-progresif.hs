-- penghasilan kena pajak
pkp :: Int
pkp = 1000000000

ppi :: Int -> Int
ppi pkpr = round(pkpr pkp)
  where 
    pkpr pkp
      | pkp <=  50000000 = fromIntegral pkp * 0.05
      | pkp <= 250000000 = fromIntegral pkp * 0.15 -  5000000
      | pkp <= 500000000 = fromIntegral pkp * 0.25 - 30000000
      | otherwise        = fromIntegral pkp * 0.30 - 55000000

main = do
   print $ ppi(pkp)

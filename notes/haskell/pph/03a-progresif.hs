-- penghasilan kena pajak
pkp :: Int
pkp = 50000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Int -> Int
pph pkp
    | pkp <=  50000000 = round(fromIntegral pkp * 0.05)
    | pkp <= 250000000 = round(fromIntegral pkp * 0.15 -  5000000)
    | pkp <= 500000000 = round(fromIntegral pkp * 0.25 - 30000000)
    | otherwise        = round(fromIntegral pkp * 0.30 - 55000000)

pkpreal :: Int -> Double
pkpreal pkp
  | pkp <=  50000000 = fromIntegral pkp * 0.05
  | pkp <= 250000000 = fromIntegral pkp * 0.15 -  5000000
  | pkp <= 500000000 = fromIntegral pkp * 0.25 - 30000000
  | otherwise        = fromIntegral pkp * 0.30 - 55000000

ppint1 :: Double -> Int
ppint1 pkpr = round(pkpr)

ppint2 :: Int -> Int
ppint2 pkpr = round(pkpr)
  where pkpr = fromIntegral pkp * 0.05

main = do
   print $ pph(pkp)
   print $ ppint1(pkpreal pkp)
   print $ ppint2(pkp)

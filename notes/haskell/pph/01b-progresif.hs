-- penghasilan kena pajak
pkp :: Double
pkp = 1000000000

-- pajak penghasilan = tarif progresif pasal 17
pph :: Double -> Double
pph pkp = pkpr pkp
  where
    (t1, t2, t3)  = (50000000, 250000000, 500000000)
    pkpr pkp
      | pkp <= t1 = pkp * 0.05
      | pkp <= t2 = (pkp - t1) * 0.15 + pph t1
      | pkp <= t3 = (pkp - t2) * 0.25 + pph t2
      | otherwise = (pkp - t3) * 0.30 + pph t3

main = do
   print $ round (pph pkp)


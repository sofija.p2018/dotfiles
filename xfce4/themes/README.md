XFWM4 
=====================

# Theme Screenshots

For each theme, SVG available freely.

## tutor-01

> PNG Solid Border (Very Basic, No Design Yet)

![XFWM4 Theme: Tutor 01][ss-xfwm4-tutor-01]

## tutor-02

> XPM Solid Border

![XFWM4 Theme: Tutor 02][ss-xfwm4-tutor-02]

## tutor-03

> Minimalist PNG Button (Flat Design)

![XFWM4 Theme: Tutor 03][ss-xfwm4-tutor-03]

## tutor-04

> PNG Gradient over XPM

This work, inspired by **xfwm-gaps**. As my admiration, and respect, to his excellent taste in design.

*	[addy-dclxvi/xfwm4-theme-collections](https://github.com/addy-dclxvi/xfwm4-theme-collections)

![XFWM4 Theme: Tutor 04][ss-xfwm4-tutor-04]

## tutor-05

> PNG Fancy Icon

![XFWM4 Theme: Tutor 05][ss-xfwm4-tutor-05]

Or alternatively

![XFWM4 Theme: Tutor 05 Alternative][ss-xfwm4-tutor-05-alt]

## xfgaps

> minimalist theme, that is very suitable for tiling.

![XFWM4 Theme: XF Gaps][ss-xfwm4-xfgaps]

## xftab

This work, inspired by **ArchEdgeUI**. As my admiration, and respect, to his excellent taste in design.

*	[ArchedgeUI-Tabbed-Decoration-for-Xfce-607124366](https://www.deviantart.com/art/ArchedgeUI-Tabbed-Decoration-for-Xfce-607124366)

![XFWM4 Theme: XF Tab][ss-xfwm4-xftab]

## 2bmat

> Yet another minimalist theme

![XFWM4 Theme: 2bmat][ss-xfwm4-2bmat]

-- -- --

# License

## Public Domain Dedication

Most themes released under CC0 1.0 Universal (CC0 1.0) license.

* https://creativecommons.org/publicdomain/zero/1.0/legalcode

## General Public License

Except Dowault, as a derivative works of XFCE4's Default Theme, released under GNU GPL license.




[ss-xfwm4-tutor-01]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-01.png
[ss-xfwm4-tutor-02]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-02.png
[ss-xfwm4-tutor-03]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-03.png
[ss-xfwm4-tutor-04]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-04.png
[ss-xfwm4-tutor-05]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-05.png
[ss-xfwm4-tutor-05-alt]: https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-tutor-05-alt.png

[ss-xfwm4-xfgaps]:       https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-xfgaps.png
[ss-xfwm4-xftab]:        https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-xftabs.png
[ss-xfwm4-2bmat]:        https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-2bmat.png
[ss-xfwm4-dowaullt]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/xfce4/themes/screenshots/ss-xfwm4-dowault.png


-- Author url = http://dobbie03.deviantart.com/art/My-First-Conky-Config-327206399
-- Modified by Akza
-- 11 June 2019: Modified by epsi

-- vim: ts=4 sw=4 noet ai cindent syntax=lua

--[[
Conky, a system monitor, based on torsmo
]]

conky.config = {
-- common
  alignment = 'middle_middle',
  background = false,
  double_buffer = true,
  total_run_times = 0,
  update_interval = 1,
  default_color = '#ffffff',

-- window
  own_window = true,
  own_window_title = 'conky',
  own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
  own_window_transparent = true,
  own_window_type = 'normal',
  own_window_argb_value = 0,
  own_window_argb_visual = true,
  own_window_colour = '#000000',
  
-- font  
  use_xft = true
}

conky.text = [[
${font CabinSketch-Bold:pixelsize=15}${alignc}${time [ %A, %I:%M:%S ]}${font}
${font CabinSketch-Bold:pixelsize=15}${alignc}${time %d %B, %Y}${font}
${font CabinSketch-Bold:pixelsize=45}${alignc}Let's Get Married!${font}
]]

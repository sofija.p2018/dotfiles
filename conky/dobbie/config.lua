configuration = {
-- common
  alignment = 'middle_middle',
  background = false,
  double_buffer = true,
  total_run_times = 0,
  update_interval = 1,

-- border
  border_inner_margin = 5,
  border_outer_margin = 5,
  border_width = 5,

-- color
  -- #413536 # grey 5f5f5f 3F3F3F 183149 3B3B3B 26211F
  default_color = '#ffffff',

-- draw options
  draw_borders = false,
  draw_outline = false,
  draw_shades = false,

-- positioning
  gap_x = 0,
  gap_y = 0,
  maximum_width = 2000,
  minimum_height = 0,
  minimum_width = 0,

  no_buffers = true,
  override_utf8_locale = true,

-- window
  own_window = true,
  own_window_title = 'conky',
  own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
  own_window_transparent = true,

-- 'normal', 'dock', 'panel', 'desktop', 'override'
  own_window_type = 'normal',

  own_window_argb_value = 0,
  own_window_argb_visual = true,
  own_window_colour = '#000000',

-- text
  text_buffer_size = 8000,
  uppercase = false,
  
-- font  
  use_xft = true,
  xftalpha = 1,
  xftfont = 'Freesans:pixelsize=9'

--				fonts
--	Blue Highway
--	Zegoe Light - U
--	Zekton
--	Calibri
--	Engebrechtre
--	Opeln2001
--	Pricedown
}

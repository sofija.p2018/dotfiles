-- Author url = http://dobbie03.deviantart.com/art/My-First-Conky-Config-327206399
-- Modified by Akza
-- 11 June 2019: Modified by epsi

-- vim: ts=4 sw=4 noet ai cindent syntax=lua

--[[
Conky, a system monitor, based on torsmo
]]

local dirname  = debug.getinfo(1).source:match("@?(.*/)")
dofile(dirname .. '/config.lua')
conky.config = configuration

datetime = ''
.. '${font CabinSketch-Bold:pixelsize=15}'
.. '${alignc}'
.. '${time [ %A, %I:%M:%S ]}'
.. '\n'
.. '${font CabinSketch-Bold:pixelsize=20}'
.. '${alignc}'
.. '${exec ~/conky/conkyhijri.sh}'
.. '\n'

message = ''
.. "${font CabinSketch-Bold:pixelsize=45}"
.. "${alignc}"
.. "Let's Get Married!"
.. "\n"

cpu_usage = 'CPU Usage: ${cpu}% - RAM Usage: ${mem}'

root = 'Root: ${fs_free /} / ${fs_size /}'

battery = 'Battery: ${battery_percent BAT0}% -'
.. ' Remaining Time: ${battery_time BAT0} '

user = 'User: ${exec users} - System Uptime: ${uptime_short}'

wlan = 'wlp3s0'

net_up = 'Net Up: '
.. '${if_existing /proc/net/route ' .. wlan .. '}'
..   '${upspeed ' .. wlan .. '}'
.. '${else}'
..   '${if_existing /proc/net/route eth0}'
..     '${upspeed ' .. wlan .. '}'
..   '${endif}'
.. '${endif}'

net_down = 'Net Down: '
.. '${if_existing /proc/net/route ' .. wlan .. '}'
..   '${downspeed ' .. wlan .. '}'
.. '${else}'
..   '${if_existing /proc/net/route eth0}'
..     '${downspeed ' .. wlan .. '}'
..   '${endif}'
.. '${endif}'

monitor = ''
.. '${font CabinSketch-Bold:pixelsize=12}'
.. '${alignc}[ ' .. cpu_usage .. ' ]\n'
.. '${alignc}[ ' .. root .. ' ]\n'
-- '${alignc}[ ' .. battery .. ' ]\n'
.. '${alignc}[ ' .. user .. ' ]\n'
.. '${alignc}[ ' .. net_up .. ' ' ..net_down .. ' ]\n'

conky.text = ''
.. datetime
.. message 
.. ''
.. monitor

# Example from http://openbox.org/wiki/Help:Themes
# 
# Hints: to change material color.
#        use search and replace from a selection, 

# -- -- -- -- -- Window geometry
padding.width: 2
padding.height: 2
border.width: 1
window.client.padding.width: 1
window.client.padding.height: 0
window.handle.width: 3

# -- -- -- -- -- Menu geometry
menu.border.width: 1
menu.overlap.x: 0
menu.overlap.y: 0

# -- -- -- -- -- Border colors

!! green700
window.active.border.color: #388e3c

!! grey500
window.inactive.border.color: #9e9e9e

!! green700
menu.border.color: #388e3c

!! white
window.active.client.color: #ffffff

!! black
window.inactive.client.color: #000000

# -- -- -- -- -- Text shadows
window.active.label.text.font: shadow=y:shadowtint=20:shadowoffset=1
window.inactive.label.text.font: shadow=y:shadowtint=70:shadowoffset=1
menu.items.font: shadow=n
menu.title.text.font: shadow=y:shadowtint=20:shadowoffset=1

# -- -- -- -- -- Window title justification
window.label.text.justify: Center

# -- -- -- -- -- Active window

!! window.active.label.bg: Parentrelative

!! white on grey100 to grey300
window.active.title.bg: Raised Gradient SplitVertical
window.active.title.bg.color: #f5f5f5
window.active.title.bg.colorTo: #e0e0e0
window.active.label.text.color: #ffffff

!! green100 on green300 to green700, with green900
window.active.label.bg: Flat Gradient SplitVertical Border
window.active.label.bg.color: #81c784
window.active.label.bg.colorTo: #388e3c
window.active.label.bg.border.color: #1b5e20

!! green100
window.active.handle.bg: Solid Flat
window.active.handle.bg.color: #c8e6c9

!! green900
window.active.grip.bg: Solid Flat
window.active.grip.bg.color: #1b5e20

!! green100 on green300 to green700, with green900
window.active.button.unpressed.bg: Flat Gradient SplitVertical Border
window.active.button.unpressed.bg.color: #81c784
window.active.button.unpressed.bg.colorTo: #388e3c
window.active.button.unpressed.bg.border.color: #1b5e20
window.active.button.unpressed.image.color: #c8e6c9

!! green100 on green900, with black
window.active.button.pressed.bg: Solid Flat Border
window.active.button.pressed.bg.color: #1b5e20
window.active.button.pressed.bg.border.color: #000000
window.active.button.pressed.image.color: #c8e6c9

!! green100 on green300 to green500, with green700
window.active.button.disabled.bg: Flat Gradient SplitVertical Border
window.active.button.disabled.bg.color: #81c784
window.active.button.disabled.bg.colorTo: #4caf50
window.active.button.disabled.bg.border.color: #388e3c
window.active.button.disabled.image.color: #c8e6c9

!! green100 on green500 to green700, with black
window.active.button.hover.bg: Flat Gradient SplitVertical Border
window.active.button.hover.bg.color: #4caf50
window.active.button.hover.bg.colorTo: #388e3c
window.active.button.hover.bg.border.color: #000000
window.active.button.hover.image.color: #c8e6c9

!! green100 on green700 to green300, with blu900
window.active.button.toggled.unpressed.bg: Flat Gradient SplitVertical Border
window.active.button.toggled.unpressed.bg.color: #388e3c
window.active.button.toggled.unpressed.bg.colorTo: #81c784
window.active.button.toggled.unpressed.bg.border.color: #1b5e20
window.active.button.toggled.unpressed.image.color: #c8e6c9

!! green900 on green300, with white
window.active.button.toggled.pressed.bg: Solid Flat Border
window.active.button.toggled.pressed.bg.color: #c8e6c9
window.active.button.toggled.pressed.bg.border.color: #ffffff
window.active.button.toggled.pressed.image.color: #1b5e20

!! green100 on green700 to green500, with black
window.active.button.toggled.hover.bg: Flat Gradient SplitVertical Border
window.active.button.toggled.hover.bg.color: #388e3c
window.active.button.toggled.hover.bg.colorTo: #4caf50
window.active.button.toggled.hover.bg.border.color: #000000
window.active.button.toggled.hover.image.color: #c8e6c9

# -- -- -- -- -- Inactive windows

!! grey700 on grey100 to grey300
window.inactive.title.bg: Raised Gradient SplitVertical
window.inactive.title.bg.color: #f5f5f5
window.inactive.title.bg.colorTo: #e0e0e0
window.inactive.label.text.color: #616161

window.inactive.label.bg: Parentrelative

!! grey700
window.inactive.handle.bg: Solid Flat
window.inactive.handle.bg.color: #616161

!! grey900
window.inactive.grip.bg: Solid Flat
window.inactive.grip.bg.color: #212121

!! grey100 on grey500 to grey700, with grey300
window.inactive.button.unpressed.bg: Flat Gradient SplitVertical Border
window.inactive.button.unpressed.bg.color: #9e9e9e
window.inactive.button.unpressed.bg.colorTo: #616161
window.inactive.button.unpressed.bg.border.color: #e0e0e0
window.inactive.button.unpressed.image.color: #f5f5f5

!! grey100 on grey300 to grey500, with grey300
window.inactive.button.disabled.bg: Flat Gradient SplitVertical Border
window.inactive.button.disabled.bg.color: #e0e0e0
window.inactive.button.disabled.bg.colorTo: #9e9e9e
window.inactive.button.disabled.bg.border.color: #e0e0e0
window.inactive.button.disabled.image.color: #f5f5f5

!! grey100 on grey700 to grey500, with grey300
window.inactive.button.toggled.unpressed.bg: Flat Gradient SplitVertical Border
window.inactive.button.toggled.unpressed.bg.color: #616161
window.inactive.button.toggled.unpressed.bg.colorTo: #9e9e9e
window.inactive.button.toggled.unpressed.bg.border.color: #e0e0e0
window.inactive.button.toggled.unpressed.image.color: #f5f5f5


# -- -- -- -- -- Menus
!! black on green300 to green700, with green900
menu.title.bg: Flat Gradient SplitVertical
menu.title.bg.color: #81c784
menu.title.bg.colorTo: #388e3c
!! menu.title.bg.border.color: #1b5e20
menu.title.text.color: #000000
menu.title.text.justify: Left

!! green900
menu.separator.color: #1b5e20
menu.separator.width: 1
menu.separator.padding.width: 6
menu.separator.padding.height: 3

!! black on grey100, except grey500
menu.items.bg: Flat Solid
menu.items.bg.color: #f5f5f5
menu.items.text.color: #000000
menu.items.disabled.text.color: #9e9e9e

!! black on green500 to grey100, except grey500
menu.items.active.bg: Flat Gradient Horizontal
menu.items.active.bg.color: #4caf50
menu.items.active.bg.colorTo: #f5f5f5
menu.items.active.text.color: #000000
menu.items.active.disabled.text.color: #9e9e9e


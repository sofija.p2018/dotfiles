# Flatypuss : The Flat Cat Openbox Theme
#
#    With my respect for platypus
#    We still love you!
#
# Structure: based on example from 
#    * http://openbox.org/wiki/Help:Themes
# 
# Boxed Icon Trick: based on Addy's Block Theme
#    * https://github.com/addy-dclxvi/openbox-theme-collections/tree/master/Blocks/openbox-3
#
# Menu Border Trick: based on Yuga's Arc Theme
#    * https://github.com/noirecat/arc-ob-theme

# -- -- -- -- -- Window geometry
padding.width:  2
padding.height: 2
border.width:   1
window.client.padding.width:  1
window.client.padding.height: 0
window.handle.width: 3

# -- -- -- -- -- Menu geometry
menu.border.width: 7
menu.overlap.x:    -14
menu.overlap.y:    0

# -- -- -- -- -- Border colors

!! grey100
window.active.border.color:   #f5f5f5

!! grey500
window.inactive.border.color: #9e9e9e

!! grey100
menu.border.color:            #f5f5f5

!! grey100
window.active.client.color:   #f5f5f5

!! black
window.inactive.client.color: #000000

# -- -- -- -- -- Text shadows
window.active.label.text.font: shadow=y:shadowtint=20:shadowoffset=1
window.inactive.label.text.font: shadow=y:shadowtint=20:shadowoffset=1
menu.items.font: shadow=n
menu.title.text.font: shadow=y:shadowtint=20:shadowoffset=1

# -- -- -- -- -- Window title justification
window.label.text.justify: Center

# -- -- -- -- -- Active window

!! black on grey100
window.active.title.bg:         Solid Flat
window.active.title.bg.color:   #f5f5f5

window.active.label.bg:         Parentrelative
window.active.label.text.color: #000000

!! grey900
window.active.handle.bg:        Solid Flat
window.active.handle.bg.color:  #212121

!! grey100
window.active.grip.bg:          Solid Flat
window.active.grip.bg.color:    #f5f5f5

!! grey300 on white
window.active.button.unpressed.bg:          Solid Flat
window.active.button.unpressed.bg.color:    #ffffff
window.active.button.unpressed.image.color: #e0e0e0

!! parentrelative: grey300 on white
window.active.button.hover.bg:              parentrelative
window.active.button.pressed.bg:            parentrelative
window.active.button.disabled.bg:           parentrelative
window.active.button.toggled.unpressed.bg:  parentrelative
window.active.button.toggled.pressed.bg:    parentrelative
window.active.button.toggled.hover.bg:      parentrelative

!! teal: u:300, h:500, p:700, d:grey300, tu:300, th:500, tp: 700
window.active.button.shade.unpressed.image.color:   #4db6ac
window.active.button.shade.hover.image.color:       #009688
window.active.button.shade.pressed.image.color:     #00796b
window.active.button.shade.disabled.image.color:    #e0e0e0
window.active.button.shade.toggled.unpressed.image.color:   #4db6ac
window.active.button.shade.toggled.hover.image.color:       #009688
window.active.button.shade.toggled.pressed.image.color:     #00796b

!! orange: u:300, h:500, p:700, d:grey300, tu:300, th:500, tp: 700
window.active.button.desk.unpressed.image.color:    #ffb74d
window.active.button.desk.hover.image.color:        #ff9800
window.active.button.desk.pressed.image.color:      #f57c00
window.active.button.desk.disabled.image.color:     #e0e0e0
window.active.button.desk.toggled.unpressed.image.color:    #ffb74d
window.active.button.desk.toggled.hover.image.color:        #ff9800
window.active.button.desk.toggled.pressed.image.color:      #f57c00

!! green: u:300, h:500, p:700, d:grey300
window.active.button.iconify.unpressed.image.color: #81c784
window.active.button.iconify.hover.image.color:     #4caf50
window.active.button.iconify.pressed.image.color:   #388e3c
window.active.button.iconify.disabled.image.color:  #e0e0e0

!! blue: u:300, h:500, p:700, d:grey300, tu:300, th:500, tp: 700
window.active.button.max.unpressed.image.color:     #64b5f6
window.active.button.max.hover.image.color:         #2196f3
window.active.button.max.pressed.image.color:       #1976d2
window.active.button.max.disabled.image.color:      #e0e0e0
window.active.button.max.toggled.unpressed.image.color:     #64b5f6
window.active.button.max.toggled.hover.image.color:         #2196f3
window.active.button.max.toggled.pressed.image.color:       #1976d2

!! red: u:300, h:500, p:700, d:grey300
window.active.button.close.unpressed.image.color:   #e57373
window.active.button.close.hover.image.color:       #f44336
window.active.button.close.pressed.image.color:     #d32f2f
window.active.button.close.disabled.image.color:    #e0e0e0

# -- -- -- -- -- Inactive windows

!! grey500 on grey100
window.inactive.title.bg:         Solid Flat
window.inactive.title.bg.color:   #f5f5f5

window.inactive.label.bg:         Parentrelative
window.inactive.label.text.color: #e0e0e0

!! grey100
window.inactive.handle.bg:        Solid Flat
window.inactive.handle.bg.color:  #f5f5f5

!! grey900
window.inactive.grip.bg:          Solid Flat
window.inactive.grip.bg.color:    #212121

!! grey100 on grey300
window.inactive.button.unpressed.bg:                  Solid Flat
window.inactive.button.unpressed.bg.color:            #e0e0e0
window.inactive.button.unpressed.image.color:         #f5f5f5

!! grey100 on grey700
window.inactive.button.disabled.bg:                   Solid Flat
window.inactive.button.disabled.bg.color:             #616161
window.inactive.button.disabled.image.color:          #f5f5f5

!! grey100 on grey300
window.inactive.button.toggled.unpressed.bg:          Solid Flat
window.inactive.button.toggled.unpressed.bg.color:    #e0e0e0
window.inactive.button.toggled.unpressed.image.color: #f5f5f5

!! teal: u:100
window.inactive.button.shade.unpressed.image.color:   #b2dfdb

!! orange: u:100
window.inactive.button.desk.unpressed.image.color:    #ffe0b2

!! green: u:100
window.inactive.button.iconify.unpressed.image.color: #c8e6c9

!! blue: u:100
window.inactive.button.max.unpressed.image.color:     #bbdefb

!! red: u:100
window.inactive.button.close.unpressed.image.color:   #ffcdd2

# -- -- -- -- -- Menus

!! black on red100, with grey100
menu.title.bg: Solid Flat Border
menu.title.bg.color: #ffcdd2
menu.title.bg.border.color: #f5f5f5
menu.title.text.color: #000000
menu.title.text.justify: Center

!! grey700
menu.separator.color:           #616161
menu.separator.width:           3
menu.separator.padding.width:   6
menu.separator.padding.height:  3

!! black on grey100, except grey500
menu.items.bg:                  Solid Flat
menu.items.bg.color:            #f5f5f5
menu.items.text.color:          #000000
menu.items.disabled.text.color: #9e9e9e

!! white on red300, with grey500, except grey500
menu.items.active.bg:                  Solid Flat Border
menu.items.active.bg.color:            #e57373
menu.items.active.bg.border.color:     #9e9e9e
menu.items.active.text.color:          #ffffff
menu.items.active.disabled.text.color: #9e9e9e

